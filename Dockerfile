FROM node:lts-alpine

# Create app directory
WORKDIR /usr/src/freyr

# Bundle app source
COPY . .

# Install buildtools
RUN apk add python make gcc g++

# Install npm modules specified in package.json
RUN npm install

# Expose container's port 4000
EXPOSE 4000

# Launch Freyr server
CMD ["node", "index.js"]
