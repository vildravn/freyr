/**
 * Constants
 */

// The port the webserver is listening on
const SERVER_PORT = process.env.SERVER_PORT || 4000;

// Type of the sensor
// 11 - DHT11
// 22 - DHT22 or AM2302
const SENSOR_TYPE = process.env.SENSOR_TYPE || 11;

// GPIO pin of the Pi where the sensor is connected
const GPIO_PIN = process.env.GPIO_PIN || 4;

// If TEST_MODE is true, sensor library is using fake data
const TEST_MODE = process.env.TEST_MODE || false;
const TEST_MODE_TEMP = process.env.TEST_MODE_TEMP || 21;
const TEST_MODE_HUM = process.env.TEST_MODE_HUM || 60;

/**
 * Module dependencies
 */

var express = require('express');
var path = require('path');
var sensor = require("node-dht-sensor");

var app = express();

if (TEST_MODE) {
    sensor.initialize({
        test: {
            fake: {
                temperature: Number(TEST_MODE_TEMP),
                humidity: Number(TEST_MODE_HUM)
            }
        }
    });
}

// Register ejs as .html. If we did
// not call this, we would need to
// name our views foo.ejs instead
// of foo.html. The __express method
// is simply a function that engines
// use to hook into the Express view
// system by default, so if we want
// to change "foo.ejs" to "foo.html"
// we simply pass _any_ function, in this
// case `ejs.__express`.

app.engine('.html', require('ejs').__express);

// Optional since express defaults to CWD/views

app.set('views', path.join(__dirname, 'views'));

// Path to our public directory

app.use(express.static(path.join(__dirname, 'public')));

// Without this you would need to
// supply the extension to res.render()
// ex: res.render('users.html').
app.set('view engine', 'html');

let dht = {
    temp: null,
    hum: null
}

app.get('/', function (req, res) {
    sensor.read(SENSOR_TYPE, GPIO_PIN, function (err, t, h) {
        if (!err) {
            dht.temp = t;
            dht.hum = h;
        }
    });
    res.render('output', {
        dht: dht,
        title: "Freyr Weather Station"
    });
});

/* istanbul ignore next */
if (!module.parent) {
    app.listen(SERVER_PORT);
    console.log(`Freyr started on port ${SERVER_PORT}`);
}